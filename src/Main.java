import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        System.out.println("Введите число 1, 2 или 3: ");
        Scanner inputFigure = new Scanner(System.in);
        int x = inputFigure.nextInt();
        task1(x);
        task1_1(x);
        task2();
        task3();
        task4();
        task5();
    }

    public static void task1(int i) {
        if(i==1) {
            System.out.println(1);
        }
        else if(i==2) {
            System.out.println(2);
        }
        else if(i==3){
            System.out.println(3);
        }
        else {
            System.out.println("Incorrect number");
        }
    }

    public static void task1_1(int i) {
        switch (i){
            case 1:
                System.out.println(1); break;
            case 2:
                System.out.println(2); break;
            case 3:
                System.out.println(3); break;
            default:
                System.out.println("Incorrect number");
        }
    }
    public static void task2() {
        for (int x=5; x>=1; x--) {
            System.out.print(x + " ");
        }
        System.out.println();
    }
    public static void task3() {
        int x = 3;
            for (int i = 1; i <= 10; i++)
        {
            System.out.println(x+"*"+i+"=" + x * i);
        }
    }
    public static void task4() {
        int sum = 0;
        for (int a = 1; a<=100; a++){
            sum+=a;
        }
        System.out.println(sum/2);
    }
    public static void task5() {
        int [] array = {5,2,4,8,88,22,10};
        int max = array[0];
        for (int i = 0; i<array.length; i++) {
            if (array[i]>max){
             max=array[i];
            }
        }
        System.out.println(max);
    }
}


